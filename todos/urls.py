from django.urls import path
from todos.views import todos_list, details_of_todo_list, todo_list_create
from todos.views import todo_list_update, todo_list_delete

urlpatterns = [
    path("", todos_list, name="todo_list_list"),
    path("<int:id>/", details_of_todo_list, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
]
