from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


# Create your views here.
def todos_list(request):
    list_of_todos = TodoList.objects.all()
    context = {
        "list_of_todos": list_of_todos
    }
    return render(request, "todos/list.html", context)


def details_of_todo_list(request, id):
    details = TodoList.objects.get(id=id)
    print(details)
    context = {
        "details": details,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = TodoListForm(instance=update)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todos/list.html")

    return render(request, "todos/delete.html")
